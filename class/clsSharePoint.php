<?php 

/**
 * 
 */

require_once 'vendor/autoload.php';

use Office365\Runtime\Auth\UserCredentials;
use Office365\SharePoint\ClientContext;
use Office365\SharePoint\FileCreationInformation;

class clsSharePoint 
{
	var $config;
	var $url;
	var $mysql;
	var $session_id;
	var $login_parameters;	
	var $adjuntos;	
	var $drive_parent;
	var $hora_inicio;
	var $http_code;

	function __construct()
	{
		try{
			$this->hora_inicio  = new DateTime();
			
			require_once("conf/conf.php");
			$this->config = $config;
			
			require_once("dbMysql.php");
			$this->mysql=new dbMysql();
			$link = $this->mysql->connect($this->config["mysql"]);
			$this->login_parameters = array(
				"grant_type"=>$this->config['sugar']['grant_type'], 
				"client_id"=>$this->config['sugar']['client_id'], 
				"client_secret"=>$this->config['sugar']['client_secret'], 
				"username"=>$this->config['sugar']['username'], 
				"password"=>$this->config['sugar']['password'], 
				"platform"=>$this->config['sugar']['platform']
			);
		} 
		catch (Exception $e) {     
			echo "\nERROR5:".$e->getMessage()."\n";
		}
	}

	function elapsed_time(){
		$first = $this->hora_inicio;
		$second = new DateTime();

		$diff = $first->diff( $second );

		echo "\nelapsed_time: ".$diff->format( '%H:%I:%S' )."\n"; // -> 00:25:25
	}

	function obtenerAdjuntos($modulo)
	{		
		try{	
			echo "\n\n--obtenerAdjuntos({$this->config['file']['cantidad']})--\n\n";$this->elapsed_time();
			$return = true;	
			echo "\n Autenticando Sugar \n";	
			if($this->wsSugarLogin(true)){
				$this->url ="{$this->config['sugar']['site_url']}{$this->config['endpoint']['sharespoint_ws']}"; 
				$dato = array(
					"obtenerAdjuntos"=>$this->config['file']['obtenerAdjuntos'],
					"cantidad"=>$this->config['file']['cantidad'],
					"modulo"=>$modulo
				);
				$result=$this->wsSugarEnviar($dato,"POST","obtenerNotas");	
				$this->adjuntos = json_decode($result);

				if(!empty($this->adjuntos)){		

					if($this->adjuntos->status == 200){
						if(!empty($this->adjuntos->result) and $this->adjuntos->status == 200 and $this->adjuntos->result != "Sin adjuntos que procesar"){	
							shell_exec("mkdir -p {$this->config['file']['files_path_local']}");//creando directorio si no existe
							if($this->config['file']['delete_files_before_run']){
								echo "\nBorrando {$this->config['file']['files_path_local']}/*\n";
								shell_exec("rm -Rf {$this->config['file']['files_path_local']}/*");///borrando contenido directorio antes de empesar								
							}
							foreach($this->adjuntos->result as $key => $adjunto){
								if ($modulo=='Documentos') {
									$this->url ="{$this->config['sugar']['site_url']}{$this->config['endpoint']['documents_ws1']}{$adjunto->id}{$this->config['endpoint']['documents_ws2']}";
								}elseif ($modulo=='Notas') {
									$this->url ="{$this->config['sugar']['site_url']}{$this->config['endpoint']['note_ws1']}{$adjunto->id}{$this->config['endpoint']['note_ws2']}"; 
								}

								$content=$this->wsSugarEnviar(null,"GET","obtenerAdjuntos");
								
								if ($this->http_code != '404') {
									if(empty($content)){
										echo "\nContenido vacio para el adjunto {$adjunto->filename} de id {$adjunto->id}";
									}
									try{
										shell_exec("mkdir -p {$this->config['file']['files_path_local']}{$adjunto->id}");//creando directorio si no existe								
										$file = fopen("{$this->config['file']['files_path_local']}{$adjunto->id}/{$adjunto->filename}", "{$this->config['file']['files_path_local_permiso']}");
										if ($file === false) {
											echo "\nERROR19: Failed to set file {$adjunto->filename} de id {$adjunto->id}";	
										}
										else{
											echo "\nCargando adjunto local {$adjunto->id} nombre: {$adjunto->filename}";
											fwrite($file, $content);
										}
										fclose($file);
										
										if (!file_exists("{$this->config['file']['files_path_local']}{$adjunto->id}/{$adjunto->filename}")) {	
											echo "\nERROR32: Error al revisar archivo descargado de SugarCRM {$this->config['file']['files_path_local']}/{$adjunto->id}/{$adjunto->id}\n";
										}
									}
									catch (Exception $e) {   
										echo "\nERROR20:".$e->getMessage()."\n";
									}
								}else{
									echo "\n\n  Error: el WS entregó un status {$this->http_code}\n";
									$return=false;
									$this->url ="{$this->config['sugar']['site_url']}/rest/v11_2/Documents/{$adjunto->parent_id}";
									$data = array("tag"=>array("Borrado Adjunto Fallo"));
									$resulta=$this->wsSugarEnviar($data,"PUT","obtenerNotas");
								}
																		
							}
						}
						else if($this->adjuntos->result == "Sin adjuntos que procesar"){							
							echo "\n\n  Estado {$this->adjuntos->status} {$this->adjuntos->result}\n";
							$return=false;
						}
						else{							
							echo "\n\n  Error: Estado {$this->adjuntos->status} con resultado vacio\n";
							$return=false;
						}
					}
					else{
						echo "\n  Error: Estatus WS {$this->http_code} \n";
						$return=false;
					}
				}
				else{
					echo "\n Adjuntoa vacio";
					$return=false;
				}
			}
			else{
				$return=false;
			}
			return $return;
		} 
		catch (Exception $e) {     
			echo "\nERROR21:".$e->getMessage()."\n";
		}
	}

	function marcarAdjuntosEnSugar($modulo)
	{		
		try{
			echo "\n\n--marcarAdjuntosEnSugar()--\n\n";$this->elapsed_time();
			$return = true;	
			if($this->wsSugarLogin(false)){
				if(!empty($this->adjuntos->result)){	
					$this->url ="{$this->config['sugar']['site_url']}{$this->config['endpoint']['sharespoint_ws']}"; 
					$dato = array(
						"marcarAdjuntosEnSugar"=>$this->config['file']['marcarAdjuntosEnSugar'],
						"registros"=>$this->adjuntos->result,
						"etiqueta_borrar_adjunto"=>$this->config['file']['etiqueta_borrar_adjunto'],
						"modulo"=>$modulo,
					);
					$result=$this->wsSugarEnviar($dato,"POST","marcarAdjuntosEnSugar");	
					$respuesta = json_decode($result);
					echo "URL MARCAR ".$this->url;
					if(!empty($respuesta)){	
						foreach($respuesta->result as $key => $adjunto){
							$this->adjuntos->result->$key->etiqueta =  ($respuesta->status == 200)? print_r($adjunto->mensaje,true) : "--";
							$this->adjuntos->result->$key->marcarAdjuntosEnSugar = ($respuesta->status == 200)? "Si" : "No";
						}					
						if($respuesta->status != 200){	
							echo "\nERROR6: fallo marcarAdjuntosEnSugar() ".print_r($respuesta->result,true)."\n";
							$return=false;
						}
					}
					else{
						$return=false;
					}
				}
				else{
					$return=false;
				}
			}
			else{
				$return=false;
			}
			return $return;
		} 
		catch (Exception $e) {     
			echo "\nERROR7:".$e->getMessage()."\n";
		}
	}

	function EnviarAdjuntos(){
		try {
			echo "\n\n--enviarAdjuntos({$this->config['drive']['cantidad']})-- ";$this->elapsed_time();
			$return = true;

			if (!empty($this->adjuntos->result) and $this->adjuntos->status == 200) {
				
				foreach ($this->adjuntos->result as $key => $adjunto) {
					//Crear carpeta en Sharepoint
					$UrlFolder = $this->create_folder_sharepoint($adjunto->id,$adjunto->parent_type);
					echo "\nURLFolder ".$UrlFolder['URLFolder'];
					//Si proceso bien la creación de tarea iniciar con llevar el adjunto a sharepoint
					if ($UrlFolder['URLFolder']) {
						//Subir archivo a Sharepoint
						$UrlFile = $this->upload_file_sharepoint($adjunto->parent_type,$adjunto->id,$adjunto->filename);
						echo "\nURLFile ".$UrlFile['URLFile'];
						if ($UrlFile['URLFile']) {
							//Llevar la URL del adjunto
							$this->adjuntos->result->$key->sharepoint_url = $this->config['sharepoint']['root_url'].$UrlFile['URLFile'];
						}
					}
				}
				
			}else{
				echo "\nERROR17: Estado {$this->adjuntos->status} con resultado vacio\n";
				$return = false;
			}

		} catch (Exception $e) {
			echo "\nERROR18:".$e->getMessage()."\n";
		}
		return $return;
	}

	function upload_file_sharepoint($parent_type,$id_adjunto,$file_name){
		$return = array();
		try {
	        $credentials = new UserCredentials($this->config['sharepoint']['UserName'], $this->config['sharepoint']['Password']);
	        $ctx = (new ClientContext($this->config['sharepoint']['Url']))->withCredentials($credentials);

	        //Si la nota no esta relacionada a ningun padre se considera una nota huerfana
			if ($parent_type == null || $parent_type == "") {
				$parent_type = "Huerfano";
			}

	        $targetFolderUrl = $this->config['sharepoint']['root_directory'].'/'.$parent_type.'/'.$id_adjunto;
	        $localPath = $this->config['file']['files_path_local'].$id_adjunto.'/'.$file_name;

	        $fileName = basename($localPath);
	        //reenombrar los archivos .eml ya que sharepoint los reconocia como archivos peligrosos
			if (strpos($file_name, ".eml") !== false) {
				$fileName = "filename.txt";
			}
	        $fileCreationInformation = new FileCreationInformation();
	        $fileCreationInformation->Content = file_get_contents($localPath);
	        $fileCreationInformation->Url = $fileName;

	        $uploadFile = $ctx->getWeb()->getFolderByServerRelativeUrl($targetFolderUrl)->getFiles()->add($fileCreationInformation);
	        $ctx->executeQuery();
	        
	        $return['URLFile'] = $uploadFile->getServerRelativeUrl();
	    } catch (Exception $e) {
	        echo 'Error: ',  $e->getMessage(), "\n";
	        $return['Error'] = $e->getMessage();
	    }
		return $return;
	}

	//Funcion para procesar/validar y crear carpetas en Sharepoint
	function create_folder_sharepoint($folder,$parent_folder=null){
		$return = array();
		for ($i=1; $i <= 2; $i++) {
			//Si la nota no esta relacionada a ningun padre se considera una nota huerfana
			if ($parent_folder == null || $parent_folder == "") {
				$parent_folder = "Huerfano";
			}
			//Si es la primera ejecución se debe crear primero el parent_folder en la raiz del sitio, si el la segunda ejecución del for se debe crear una carpeta con el id de la nota
			if ($i==1) {
				$root_directory = $this->config['sharepoint']['root_directory'];
				$folderName = $parent_folder;
			}else{
				$root_directory = $this->config['sharepoint']['root_directory']."/".$parent_folder;
				$folderName = $folder;
			}
			
			//Usar phpSPO para crear carpetas en sharepoint https://github.com/vgrem/phpSPO
			try {
			    $credentials = new UserCredentials($this->config['sharepoint']['UserName'], $this->config['sharepoint']['Password']);
			    $ctx = (new ClientContext($this->config['sharepoint']['Url']))->withCredentials($credentials);

			    $rootFolder = $ctx->getWeb()->getFolderByServerRelativeUrl($root_directory);
			    $newFolder = $rootFolder->getFolders()->add($folderName)->executeQuery();
			    //print($newFolder->getServerRelativeUrl())."\r\n";
			    $return['URLFolder'] = $newFolder->getServerRelativeUrl();
			} catch (Exception $e) {
			     echo 'Error: ',  $e->getMessage(), "\n";
			     $return['Error'] = $e->getMessage();
			}
		}
		return $return;
	}

	//WS para consumir los servicios de SugarCRM
	function wsSugarEnviar($dato=null,$method,$action){//POST
		$curl_response  = "";
		try{	
			if(!empty($this->session_id)){	
				//echo "URL2: {$this->url}\n";
				$url = $this->url;
				$curl_request = curl_init($url);
				curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
				curl_setopt($curl_request, CURLOPT_HEADER, false);
				curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
				curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "{$method}");
				curl_setopt($curl_request, CURLOPT_HTTPHEADER, array(
				    "Content-Type:application/json",
				    "Cache-Control:no-cache",
				    "OAuth-Token:".$this->session_id,
				));

				if ($action=="obtenerNotas" || $action=="marcarAdjuntosEnSugar") {
					//convert arguments to json				
					$json_arguments = json_encode($dato);
					//echo "\n URL: {$url} JSON ".print_r($json_arguments,true);
					curl_setopt($curl_request, CURLOPT_POSTFIELDS, $json_arguments );
				}
				
				//execute request
				$curl_response = curl_exec($curl_request);	

				//display the created record
				//echo "\n curl_response: ".print_r($curl_response,true)."\n";
				
				if(!empty($curl_response)){
					$httpCode = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
					$this->http_code = $httpCode;
					if($httpCode != 404) {		
						//decode json
						$createdRecord = json_decode($curl_response);
						if((!empty($createdRecord->error) && $action=="obtenerNotas") || (!empty($createdRecord->error) && $action=="marcarAdjuntosEnSugar")){
							echo "\nERROR25->wsSugarEnviar: {$url} estado:{$httpCode} genero error {$createdRecord->error}: {$createdRecord->error_message}\n";
							echo "\n\n\n".print_r($createdRecord,true)."\n\n\n";
						}elseif (!empty($createdRecord->error) && $action=="obtenerAdjuntos") {
							echo "\nERROR22->wsSugarObtener: {$url} estado:{$httpCode} genero error {$createdRecord->error}: {$createdRecord->error_message}\n";
						}
					}
					else{
						echo "\nERROR26->wsSugarEnviar: {$url} entrego error {$httpCode}\n";
					}
				}	
				
				curl_close($curl_request);	
			}	
		} 
		catch (Exception $e) {     
			echo "\nERROR27:".$e->getMessage()."\n";
		}
		return $curl_response;
	}

	//Funcion para consumir el servicio del token de SugarCRM
	function wsSugarLogin($imprimir){
		try{
			echo "\n\n--wsSugarLogin()--\n\n";
			$this->session_id = "";
			$this->url ="{$this->config['sugar']['site_url']}{$this->config['endpoint']['autenticar_ws']}"; 
			$auth_request = curl_init($this->url);
			curl_setopt($auth_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
			curl_setopt($auth_request, CURLOPT_HEADER, false);
			curl_setopt($auth_request, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($auth_request, CURLOPT_FOLLOWLOCATION, 0);
			curl_setopt($auth_request, CURLOPT_HTTPHEADER, array(
			    "Content-Type:application/json",
			    "Cache-Control:no-cache",		    
			    "OAuth-Token:{$this->session_id}",//.$this->session_id,
			));

			//convert arguments to json
			$json_arguments = json_encode($this->login_parameters);
			curl_setopt($auth_request, CURLOPT_POSTFIELDS, $json_arguments);

			//execute request
			$result_raw=curl_exec($auth_request);
			if(!empty($result_raw)){
				$result=json_decode($result_raw);
				if(!empty($result)){					
					$this->session_id = $result->access_token;
					if(!empty($this->session_id)){
						if($imprimir){
							echo "\n URL:{$this->url} \n";
							echo "\n access_token: {$this->session_id} \n";
						}
						return true;
					}
				}
			}
			echo "\nERROR28->wsSugarLogin: fallo en la api para {$this->url}.\nRESULT:".print_r($result_raw,true)."\n";
			return false;
		} 
		catch (Exception $e) {     
			echo "\nERROR29:".$e->getMessage()."\n";
		}
	}
}

 ?>