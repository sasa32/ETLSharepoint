<?php 

class dbMysql {
	protected static $connection;
	
	public function connect($c) {    
        
        if(!isset(self::$connection)) {
            
             
            self::$connection = mysqli_connect($c['host'],$c['user'],$c['password'],$c['new_link'],$c['client_flags']);
			
        }


        if(self::$connection === false) {

            return false;
        }
	mysqli_select_db(self::$connection,$c['database']);
        return self::$connection;
    }


    public function query($query) {
        // Connect to the database
        //$connection = $this -> connect();

        // Query the database
        $result =  mysqli_query(self::$connection,$query);

	if($result === false) {
            return $this->error();
        }	
		
        return $result;
    }

    public function select($query) {
        $rows = array();
        $result = $this -> query($query);
        if($result === false) {
            return $this->error();
        }
        while ($row =mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
		
        return $rows;
    }

    
    public function error() {
        $connection = $this -> connect();
        return mysqli_errno($connection);
    }

    public function quote($value) {
        $connection = $this -> connect();
        return "'" . $connection -> real_escape_string($value) . "'";
    }

	
	
	
	
}

?>