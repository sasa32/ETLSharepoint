<?php 

require_once("class/clsSharePoint.php");

$clsSharePoint=new clsSharePoint();

if($clsSharePoint->obtenerAdjuntos('Notas')){
	if ($clsSharePoint->EnviarAdjuntos()) {
		$clsSharePoint->marcarAdjuntosEnSugar('Notas');
		if(!empty($clsSharePoint->adjuntos->result)){	
			$i = 0;
			foreach($clsSharePoint->adjuntos->result as $key => $adjunto){	
				$i++;
				echo "\n";
				echo "\n->{$i}";
				echo "\n\tId Nota: {$key}";
				echo "\n\tModulo: {$adjunto->parent_type}";
				echo "\n\tPadre: {$adjunto->parent_id}";
				echo "\n\tNota: {$adjunto->name} ";
				echo "\n\tAdjunto: {$adjunto->filename}";
				echo "\n\tSharePoint url: {$adjunto->sharepoint_url}";
				echo "\n\tEtiqueta: {$adjunto->etiqueta}";
				echo "\n\tmarcarAdjuntosEnSugar: {$adjunto->marcarAdjuntosEnSugar}";
				echo "\n";
			}			
		}
		else {
			echo "\nERROR0: No hay adjuntos ".print_r($clsSharePoint->adjuntos->result,true);
		}
	}
}
$clsSharePoint->elapsed_time();

echo "\n **********************************  INICIO DOCUMENTOS ******************************\n";
unset($clsSharePoint->adjuntos);

//Procesar documentos
if($clsSharePoint->obtenerAdjuntos('Documentos')){
	if ($clsSharePoint->EnviarAdjuntos()) {
		$clsSharePoint->marcarAdjuntosEnSugar('Documentos');
		if(!empty($clsSharePoint->adjuntos->result)){	
			$i = 0;
			foreach($clsSharePoint->adjuntos->result as $key => $adjunto){	
				$i++;
				echo "\n";
				echo "\n->{$i}";
				echo "\n\tId Nota: {$key}";
				echo "\n\tModulo: {$adjunto->parent_type}";
				echo "\n\tPadre: {$adjunto->parent_id}";
				echo "\n\tNota: {$adjunto->name} ";
				echo "\n\tAdjunto: {$adjunto->filename}";
				echo "\n\tSharePoint url: {$adjunto->sharepoint_url}";
				echo "\n\tEtiqueta: {$adjunto->etiqueta}";
				echo "\n\tmarcarAdjuntosEnSugar: {$adjunto->marcarAdjuntosEnSugar}";
				echo "\n";
			}			
		}
		else {
			echo "\nERROR0: No hay adjuntos ".print_r($clsSharePoint->adjuntos->result,true);
		}
	}
}
$clsSharePoint->elapsed_time();



 ?>